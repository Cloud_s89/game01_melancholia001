﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class TriggerAction : MonoBehaviour 
{
	public UnityEvent TriggerEnter;
	public UnityEvent TriggerExit;
	public string ColliderTag = "Player";

	void OnTriggerEnter(Collider Col)
	{
		if (!Col.CompareTag (ColliderTag))
			return;

		TriggerEnter.Invoke ();
	}

	void OnTriggerExit(Collider Col)
	{
		if (!Col.CompareTag(ColliderTag))
			return;

		TriggerExit.Invoke ();
	}
}
