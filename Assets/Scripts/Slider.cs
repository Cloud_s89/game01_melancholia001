﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
//-----------------------------------------------------------------
public class Slider : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	//-----------------------------------------------------------------
	public Vector3 MinConstraint = Vector3.zero;
	public Vector3 MaxConstraint = Vector3.zero;
	private Transform ThisTransform = null;

	public Vector3 Direction = Vector3.right;
	public float Speed = 1f;

	public Rotating[] Rotators;
	private float RotateDirection = 1f;
	//-----------------------------------------------------------------
	void Awake()
	{
		ThisTransform = GetComponent<Transform> ();
	}
	//-----------------------------------------------------------------
	#region OnBeginDrag
	public void OnBeginDrag(PointerEventData eventData)
	{
		RotateObjectsLeft (false);
		RotateObjectsRight (false);
	}
	#endregion
	//-----------------------------------------------------------------
	public void OnDrag(PointerEventData eventData)
	{
		RotateDirection = -Mathf.Sign (eventData.delta.x);

		ThisTransform.localPosition += Direction * eventData.delta.x * Speed;

		//Constrain position within min and max limits
		ThisTransform.localPosition = new Vector3 (Mathf.Clamp(ThisTransform.localPosition.x, MinConstraint.x, MaxConstraint.x),
			Mathf.Clamp(ThisTransform.localPosition.y, MinConstraint.y, MaxConstraint.y),
			Mathf.Clamp(ThisTransform.localPosition.z, MinConstraint.z, MaxConstraint.z));

		if (RotateDirection < 0)
			RotateObjectsLeft (true);
		else
			RotateObjectsRight (true);
	}
	//-----------------------------------------------------------------
	public void OnEndDrag(PointerEventData eventData)
	{
		RotateObjectsLeft (false);
		RotateObjectsRight (false);
	}
	//-----------------------------------------------------------------
	public void RotateObjectsLeft(bool RotateBool)
	{
		foreach (Rotating R in Rotators) 
		{
			R.RotateLeft = RotateBool;
		}
	}
	//-----------------------------------------------------------------
	public void RotateObjectsRight(bool RotateBool)
	{
		foreach (Rotating R in Rotators) 
		{
			R.RotateRight = RotateBool;
		}
	}
	//-----------------------------------------------------------------
}
