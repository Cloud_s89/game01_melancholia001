﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
	//public string DestinationScene;

	public void LoadSceneNum (int num) {
		if (num < 0 || num >= SceneManager.sceneCountInBuildSettings) 
		{
			Debug.LogWarning ("cant load scene number" + num + ", SceneManager only has" + SceneManager.sceneCountInBuildSettings + " scenes in build settings!");
			return;
		
		}
		SceneManager.LoadScene(num);	}
	
	// Update is called once per frame

}
