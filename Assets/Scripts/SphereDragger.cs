﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class SphereDragger : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	private Transform ThisTransform = null;
	public Vector3 RotateAxis = Vector3.zero;
	public Rotating[] Rotators;
	private float RotateDirection = 1f;
	private AudioSource ThisAudio = null;

	//RotateObjects
	void Awake()
	{
		ThisTransform = GetComponent<Transform> ();
		ThisAudio = GetComponent<AudioSource> ();
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		Debug.Log ("hello");

		RotateDirection = -Mathf.Sign (eventData.delta.x);
		RotateObjectsLeft (false);
		RotateObjectsRight (false);
		ThisAudio.Play ();
	}

	public void OnDrag(PointerEventData eventData)
	{
		ThisTransform.Rotate (RotateAxis.x * RotateDirection * Mathf.Abs(eventData.delta.x), 
		RotateAxis.y * RotateDirection *  Mathf.Abs(eventData.delta.x), 
		RotateAxis.z * RotateDirection *  Mathf.Abs(eventData.delta.x));

		if (RotateDirection < 0)
			RotateObjectsLeft (true);
		else
			RotateObjectsRight (true);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		RotateObjectsLeft (false);
		RotateObjectsRight (false);

		ThisAudio.Stop ();
	}

	public void RotateObjectsLeft(bool RotateBool)
	{
		foreach (Rotating R in Rotators) 
		{
			R.RotateLeft = RotateBool;
		}
	}

	public void RotateObjectsRight(bool RotateBool)
	{
		foreach (Rotating R in Rotators) 
		{
			R.RotateRight = RotateBool;
		}
	}
}
