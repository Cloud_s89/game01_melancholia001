﻿using UnityEngine;
using System.Collections;

public class GizmoLocator : MonoBehaviour 
{
	private Camera RayCamera = null;
	public LayerMask LM;
	public Transform Gizmo = null;




	// Use this for initialization
	void Start () 
	{
		RayCamera = GetComponent<Camera> ();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown (0)) 
		{
			//Debug.Log (Input.mousePosition);
			Ray ray = RayCamera.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, Mathf.Infinity, LM.value)) 
			{
				//Calc new pos
				float NewX = (hit.normal.x != 0) ? hit.point.x : hit.collider.gameObject.transform.position.x;
				float NewY = (hit.normal.y != 0) ? hit.point.y : hit.collider.gameObject.transform.position.y;
				float NewZ = (hit.normal.z != 0) ? hit.point.z : hit.collider.gameObject.transform.position.z;
				Gizmo.position = new Vector3(NewX,NewY,NewZ);
		
			}
		}
	}
}
