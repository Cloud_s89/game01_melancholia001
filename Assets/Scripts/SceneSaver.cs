﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSaver : MonoBehaviour 
{
	public int SceneID = 0;

	// Use this for initialization
	void Start () 
	{
		if(SceneID > GameController.HighestReachedLevel)
			GameController.SaveLevel (SceneID);
	}
}
