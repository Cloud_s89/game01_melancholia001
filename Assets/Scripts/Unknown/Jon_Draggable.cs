﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Jon_Draggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
	public bool CanDrag = true;
    private bool _dragging;
    private int _pointerId = -50; // default to some number we know the id can never be
    private Vector3 _offset = Vector3.zero;

	private GameObject[] DragLimits;

    public enum DragDemonstrationType
    {
        Naive,
        ALittleBetter
    }
    public DragDemonstrationType DragType;

    public bool CanDragX = true;
    public bool CanDragY = true;
    public bool CanDragZ = true;

    private Plane _plane;

    private void Awake()
    {
        _plane = new Plane(Vector3.up, transform.position);

		DragLimits = GameObject.FindGameObjectsWithTag ("DragLimit");
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
		if (_dragging || !CanDrag) return;

        _pointerId = eventData.pointerId;
        _offset = transform.position - eventData.pointerCurrentRaycast.worldPosition;
        _dragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
		if (!_dragging || eventData.pointerId != _pointerId || !CanDrag) return;

        // FOR DEMONSTRATION ONLY
        switch(DragType)
        {
            case DragDemonstrationType.Naive:
                NaiveBasicDrag(eventData);
                break;
            case DragDemonstrationType.ALittleBetter:
                ALittleBetterDrag(eventData);
                break;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
		if (!_dragging || !CanDrag) return;

        if (eventData.pointerId == _pointerId)
        {
            _pointerId = -50;
            _offset = Vector3.zero;
            _dragging = false;
        }
    }

    private void NaiveBasicDrag(PointerEventData eventData)
    {
        transform.position = eventData.pointerCurrentRaycast.worldPosition;
    }

    private void ALittleBetterDrag(PointerEventData eventData)
    {
        Vector3 inputPosition = new Vector3(eventData.position.x, eventData.position.y, 1f);
        Ray ray = eventData.pressEventCamera.ScreenPointToRay(eventData.position);

        float distance;
        if (!_plane.Raycast(ray, out distance)) return;

        Vector3 newPosition = ray.GetPoint(distance) + _offset;

		if (HitLimit (newPosition))
			return;

        if (!CanDragX)
        {
            newPosition.x = transform.position.x;
        }

        if (!CanDragY)
        {
            newPosition.y = transform.position.y;
        }

        if (!CanDragZ)
        {
            newPosition.z = transform.position.z;
        }

        transform.position = newPosition;
    }

	public void Enable()
	{
		CanDrag = true;
	}

	public void Disable()
	{
		CanDrag = false;
	}

	bool HitLimit(Vector3 NewPos)
	{
		foreach (GameObject Go in DragLimits)
		{
			BoxCollider BoxColliderComp = Go.GetComponent<BoxCollider> ();

			if (BoxColliderComp.bounds.Contains (NewPos))
				return true;
		}

		return false;
	}
}
