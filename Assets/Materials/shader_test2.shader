// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33498,y:32416,varname:node_3138,prsc:2|emission-3246-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32548,y:32474,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_ChannelBlend,id:4939,x:32844,y:32618,varname:node_4939,prsc:2,chbt:0|M-4115-OUT,R-4448-RGB,G-7644-RGB,B-2285-RGB;n:type:ShaderForge.SFN_Color,id:4448,x:32273,y:32343,ptovrint:False,ptlb:Colour light 1,ptin:_Colourlight1,varname:node_4448,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9779412,c2:0.532115,c3:0.532115,c4:1;n:type:ShaderForge.SFN_Color,id:7644,x:32213,y:32514,ptovrint:False,ptlb:Colour Light 2,ptin:_ColourLight2,varname:node_7644,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7688522,c2:0.08109864,c3:0.9191176,c4:1;n:type:ShaderForge.SFN_Color,id:2285,x:32159,y:32695,ptovrint:False,ptlb:olour Light 33,ptin:_olourLight33,varname:node_2285,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2167312,c2:0.1693339,c3:0.7941176,c4:1;n:type:ShaderForge.SFN_Multiply,id:4115,x:32485,y:32773,varname:node_4115,prsc:2|A-9746-OUT,B-9746-OUT,C-2708-OUT,D-6176-OUT,E-1227-RGB;n:type:ShaderForge.SFN_NormalVector,id:9746,x:31842,y:32611,prsc:2,pt:False;n:type:ShaderForge.SFN_Color,id:5862,x:31795,y:33120,ptovrint:False,ptlb:Shadow tint,ptin:_Shadowtint,varname:node_5862,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.4411765,c3:0.4411765,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:3089,x:31795,y:33299,ptovrint:False,ptlb:Shadow boost,ptin:_Shadowboost,varname:node_3089,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:2708,x:32080,y:33029,varname:node_2708,prsc:2|A-5862-RGB,B-3089-OUT,C-3089-OUT;n:type:ShaderForge.SFN_Color,id:1227,x:31999,y:32869,ptovrint:False,ptlb:Rim Colour,ptin:_RimColour,varname:node_1227,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9779412,c2:0.9419875,c3:0.9486822,c4:1;n:type:ShaderForge.SFN_Color,id:246,x:32174,y:33238,ptovrint:False,ptlb:Light Multiplier,ptin:_LightMultiplier,varname:node_246,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:6176,x:32551,y:33159,varname:node_6176,prsc:2|A-246-RGB,B-9843-RGB;n:type:ShaderForge.SFN_Color,id:9843,x:32301,y:33403,ptovrint:False,ptlb:Light Add,ptin:_LightAdd,varname:node_9843,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7647059,c2:0.6972318,c3:0.6972318,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5536,x:32772,y:32844,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_5536,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:3246,x:33106,y:32577,varname:node_3246,prsc:2|A-4939-OUT,B-5536-RGB;proporder:7241-4448-7644-2285-5862-3089-1227-246-9843-5536;pass:END;sub:END;*/

Shader "Shader Forge/shader_test2" {
    Properties {
        _Color ("Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Colourlight1 ("Colour light 1", Color) = (0.9779412,0.532115,0.532115,1)
        _ColourLight2 ("Colour Light 2", Color) = (0.7688522,0.08109864,0.9191176,1)
        _olourLight33 ("olour Light 33", Color) = (0.2167312,0.1693339,0.7941176,1)
        _Shadowtint ("Shadow tint", Color) = (1,0.4411765,0.4411765,1)
        _Shadowboost ("Shadow boost", Float ) = 1
        _RimColour ("Rim Colour", Color) = (0.9779412,0.9419875,0.9486822,1)
        _LightMultiplier ("Light Multiplier", Color) = (1,1,1,1)
        _LightAdd ("Light Add", Color) = (0.7647059,0.6972318,0.6972318,1)
        _Texture ("Texture", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Colourlight1;
            uniform float4 _ColourLight2;
            uniform float4 _olourLight33;
            uniform float4 _Shadowtint;
            uniform float _Shadowboost;
            uniform float4 _RimColour;
            uniform float4 _LightMultiplier;
            uniform float4 _LightAdd;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float3 node_4115 = (i.normalDir*i.normalDir*(_Shadowtint.rgb*_Shadowboost*_Shadowboost)*(_LightMultiplier.rgb*_LightAdd.rgb)*_RimColour.rgb);
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 emissive = ((node_4115.r*_Colourlight1.rgb + node_4115.g*_ColourLight2.rgb + node_4115.b*_olourLight33.rgb)*_Texture_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
